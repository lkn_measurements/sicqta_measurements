%% Function to calculate bit error rate
function error =ber(dirty,clean)
    if isempty(dirty)
        error=1; 
    else
        [~,error]=biterr(dirty,clean);
    end
end