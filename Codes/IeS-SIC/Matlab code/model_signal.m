%% Function to mode channnel effects using averaging technique
% Inputs:
% rect_sig : Signal to model
% MPDU : Data packet MPDU
% spc : sampels per chip
% 
% Outputs:
% model : Modelled signal with channel effects

function model = model_signal(rect_sig,MPDU,spc)
sigstart=1;
%% Separate real and imagnary parts
sig_real=real(rect_sig);
sig_imag=imag(rect_sig);
%% Generate transmitted chips
% Preamble is 4 octets, all set to 0.
preamble = zeros(4*8, 1);

% Start-of-frame delimiter (SFD)
SFD = [1 1 1 0 0 1 0 1]'; % value from standard (see Fig. 68, IEEE 802.15.4, 2011 Revision)

SHR = [preamble; SFD];

% PHY Header (PHR)
frameLength = de2bi(length(MPDU)/8, 7);
reservedValue = 0;
PHR = [frameLength'; reservedValue];

% PHY protocol data unit:
PPDU = [SHR; PHR; MPDU];

chipLen = 32;    
% See Table 73 in IEEE 802.15.4,  2011 revision
chipMap = ...
 [1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0;
  1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0;
  0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0;
  0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1;
  0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1;
  0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0;
  1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1;
  1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1;
  1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1;
  1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1;
  0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1;
  0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0;
  0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0;
  0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1;
  1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0;
  1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0];

chips = zeros(chipLen, length(PPDU)/4);
for idx = 1:length(PPDU)/4
  % Bit to symbol mapping
  currBits = PPDU(1+(idx-1)*4 : idx*4);
  symbol = bi2de(currBits');
  
  % Symbol to chip mapping                            
	chips(:, idx) = chipMap(1+symbol, :)'; % +1 for 1-based indexing
end


%% O-QPSK modululation (part 1)
% split two 2 parallel streams, also map [0, 1] to [-1, 1]
oddChips  = chips(1:2:end)*2-1;
evenChips = chips(2:2:end)*2-1;

% Half-sine pulse filtering for 2450 MHz
pulse = sin(0:pi/spc:(spc-1)*pi/spc); % Half-period sine wave
filteredReal = pulse' * oddChips;     % each column is now a filtered pulse
filteredImag = pulse' * evenChips;    % each column is now a filtered pulse  

% O-QPSK modululation (part 2)
re = [filteredReal(:);         zeros(round(spc/2), 1)];
im = [zeros(round(spc/2), 1);  filteredImag(:)];
remod_sig=complex(re, im); % ideal signal


data_chips=chips(1:end);
pos_list_cell=cell(1,64); % 6 bits = 64 combinations

% Collect starting positions of same group of (3) symbols or 6 chips/bits
for i=1:2:length(data_chips)-5
    pos_list_cell{1,bi2de(data_chips(i:i+5),'left-msb')+1}=[pos_list_cell{1,bi2de(data_chips(i:i+5),'left-msb')+1} i];
end

avg_sig_real=zeros(64,12);
avg_sig_imag=zeros(64,12);

% Average the signal blocks based on collected positions
for iter1=1:length(pos_list_cell)
    temp=pos_list_cell{iter1};

    for iter2=1:length(temp)
        avg_sig_real(iter1,:)=avg_sig_real(iter1,:)+sig_real(sigstart+(((temp(iter2)+1)/2)-1)*spc:sigstart+(((temp(iter2)+1)/2)-1)*4+spc*3-1).';
        avg_sig_imag(iter1,:)=avg_sig_imag(iter1,:)+sig_imag(sigstart+(((temp(iter2)+1)/2)-1)*spc:sigstart+(((temp(iter2)+1)/2)-1)*4+spc*3-1).';
    end
    if iter2~=0
        avg_sig_real(iter1,:)=avg_sig_real(iter1,:)./length(temp);
        avg_sig_imag(iter1,:)=avg_sig_imag(iter1,:)./length(temp);
    end
end


model_real= zeros(1,length(sig_real(sigstart:sigstart-1+length(remod_sig))));
model_real(1:4)=sig_real(sigstart:sigstart+3);
model_real(end-5:end)=sig_real(sigstart-1+length(remod_sig)-5:sigstart-1+length(remod_sig));

model_imag= zeros(1,length(sig_imag(sigstart:sigstart-1+length(remod_sig))));
model_imag(1:4)=sig_imag(sigstart:sigstart+3);
model_imag(end-5:end)=sig_imag(sigstart-1+length(remod_sig)-5:sigstart-1+length(remod_sig));

% Iterate through signal and replace middle symbol with the corresponding
% middle symbol from the model
for iter1=1:length(pos_list_cell)
    temp=pos_list_cell{iter1};
    for iter2=1:length(temp)
        model_real(1+((temp(iter2)+1)/2-1)*spc+spc:1+((temp(iter2)+1)/2-1)*spc+spc*2-1)=avg_sig_real(iter1,spc+1:spc*2);
        model_imag(1+((temp(iter2)+1)/2-1)*spc+spc:1+((temp(iter2)+1)/2-1)*spc+spc*2-1)=avg_sig_imag(iter1,spc+1:spc*2);
    end
end

model=complex(model_real,model_imag);
model=model.';


end