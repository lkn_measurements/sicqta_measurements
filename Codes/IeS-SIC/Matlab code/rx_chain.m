%% Function to decode clean packet 
% Inputs:
% waveform_packet : packet to be decoded
% spc : sampls per chip (usually 4)
% packet_length : length of one data packet
% 
% Outputs:
% phaseCompensatedOQPSK : Packet after frequency and phase compensation (used to model channel effects)
% MPDU : Data packet MPDU
% coarseFrequencyOffset : Calculated frequency offset
% phase_est : Estimated phase offset(not used in this code)
% pass : 1 or 0 to represent CRC pass/fail 
% pkt : data packet (to calculate BER)

function [phaseCompensatedOQPSK,MPDU,coarseFrequencyOffset,phase_est,pass,pkt]= rx_chain(waveform_packet,spc,packet_length)
    if (waveform_packet==zeros(20501,1)) % length of one column
            pass=0;
            MPDU=[];
            phaseCompensatedOQPSK=[];
            coarseFrequencyOffset=[];
            phase_est=[];
            pkt=[];
    else
        %% Coarse frequency compensation of OQPSK signal using FFT
        coarseFrequencyCompensator = comm.CoarseFrequencyCompensator('Modulation', 'OQPSK', ...
              'SampleRate', spc*1e6, 'FrequencyResolution', 1);  
        [~, coarseFrequencyOffset] = coarseFrequencyCompensator(waveform_packet);
        fprintf('Estimated frequency offset = %.3f kHz\n', coarseFrequencyOffset/1000);

        % Preamble detection in complex signal
        % The 0-symbol
        R = [1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0];
        R = repmat(R,1,8);
        SFD=[1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1 0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1];
        R=[R SFD];
        oddChips  = R(1:2:end)*2-1;
        evenChips = R(2:2:end)*2-1;

        % Half-sine pulse filtering for 2450 MHz
        pulse = sin(0:pi/spc:(spc-1)*pi/spc); % Half-period sine wave
        filteredReal = pulse' * oddChips;     % each column is now a filtered pulse
        filteredImag = pulse' * evenChips;    % each column is now a filtered pulse  

        % O-QPSK modululation (part 2)
        re = [filteredReal(:);         zeros(round(spc/2), 1)];
        im = [zeros(round(spc/2), 1);  filteredImag(:)];
        waveform_pr = complex(re, im);

        H1 =comm.PhaseFrequencyOffset('FrequencyOffset',coarseFrequencyOffset,'SampleRate', spc*1e6);

        % Detect preamble
        prbdet = comm.PreambleDetector(H1(waveform_pr));
        [~,detmet_c] = prbdet(waveform_packet); 
        [~,idx_c]=max(detmet_c);%idx stores the end sample of the preamble+SFD

        if(idx_c-length(waveform_pr)+packet_length+1<=length(waveform_packet))
            data_signal=waveform_packet(idx_c-length(waveform_pr)+1:idx_c-length(waveform_pr)+packet_length+1); % one extra sample needed for decoding

            % Frequency compensation
            freq_shift=(0:length(data_signal)) ./ (spc*1e6) .* coarseFrequencyOffset;
            coarseCompensatedOQPSK=data_signal.*exp(-1i.*2.*pi.*freq_shift(1:length(data_signal)).');

            % Plot QPSK-equivalent coarsely compensated signal
            coarseCompensatedQPSK = complex(real(coarseCompensatedOQPSK(1:end-spc/(2))), imag(coarseCompensatedOQPSK(spc/(2)+1:end))); % align I and Q
            constellation2 = comm.ConstellationDiagram('XLimits', [-0.2 0.2], 'YLimits', [-0.2 0.2], ...
                                                       'Name', 'Coarse frequency compensation (QPSK-Equivalent)');
            constellation2.Position = [constellation2.Position(1:2) 300 300]; 
            constellation2(coarseCompensatedQPSK);

            %% Phase offset compensation
            [~,phase_est] = phase_offset_calc(coarseCompensatedOQPSK,spc);
            fprintf('Estimated phase offset = %.3f rad\n', phase_est);
            H3 = comm.PhaseFrequencyOffset('SampleRate', spc*1e6,'PhaseOffset',-phase_est*180/pi);
            phaseCompensatedOQPSK=H3(coarseCompensatedOQPSK);

            % Plot Phase compensated QPSK-equivalent signal
            phaseCompensatedQPSK = complex(real(phaseCompensatedOQPSK(1:end-spc/(2))), imag(phaseCompensatedOQPSK(spc/(2)+1:end))); % align I and Q
            constellation3 = comm.ConstellationDiagram('XLimits', [-0.2 0.2], 'YLimits', [-0.2 0.2], ...
                                                      'Name', 'Phase compensation (QPSK-Equivalent)');
            constellation3.Position = [constellation3.Position(1:2) 300 300]; 
            constellation3(phaseCompensatedQPSK);

            %% Timing recovery of OQPSK signal, via its QPSK-equivalent version
            symbolSynchronizer = comm.SymbolSynchronizer('Modulation', 'OQPSK', 'SamplesPerSymbol', spc);
            [syncedQPSK,~] = symbolSynchronizer(phaseCompensatedOQPSK);

            %Plot QPSK symbols (1 sample per chip)
            constellation4 = comm.ConstellationDiagram('XLimits', [-0.2 0.2], 'YLimits', [-0.2 0.2], ...
                                                       'Name', 'Timing Recovery (QPSK-Equivalent)');
            constellation4.Position = [constellation4.Position(1:2) 300 300]; 
            constellation4(syncedQPSK);
            %% Decoding and modelling
            % Once the signal has been synchronized, the next step is preamble
            % detection, which is more successful if the signal has been despreaded.
            % The next function operates on the synchronized
            % OQPSK signal, performs joint despreading, resolution of phase ambiguity
            % and preamble detection, and then outputs the MAC protocol data unit
            % (MPDU).
            [MPDU,pkt] = PHYDecoderOQPSKAfterSync_new(syncedQPSK);
            % CRC check
            pass=0.5;
            if isempty(MPDU)
                pass=0;
                return
            end
            [dataFrameMACConfig] = lrwpan.MACFrameDecoder(MPDU);
            if ~isempty(dataFrameMACConfig)
                fprintf('CRC check passed for the MAC frame.\n');
                dataFrameMACConfig 
                pass=1;
            end
        else
            pass=0;
            MPDU=[];
            phaseCompensatedOQPSK=[];
            coarseFrequencyOffset=[];
            phase_est=[];
            pkt=[];
        end

    end  
end