%% Function to calculate phase offset
% Inputs:
% rect_sig : Input signal for which the phase offset is to be calculated
% spc : samples per chip (usually 4)
% 
% Outputs:
% ampl : Estimated amplitude (not used)
% phase : Estiamted phase offset
function [ampl,phase] = phase_offset_calc(rect_sig,spc)
chipLen = 32;    
  % See Table 73 in IEEE 802.15.4,  2011 revision
  chipMap = ...
     [1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0;
      1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0;
      0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0;
      0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1;
      0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0 0 0 1 1;
      0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1 1 1 0 0;
      1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1 1 0 0 1;
      1 0 0 1 1 1 0 0 0 0 1 1 0 1 0 1 0 0 1 0 0 0 1 0 1 1 1 0 1 1 0 1;
      1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1;
      1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1;
      0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1;
      0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0;
      0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1 0 1 1 0;
      0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0 1 0 0 1;
      1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0 1 1 0 0;
      1 1 0 0 1 0 0 1 0 1 1 0 0 0 0 0 0 1 1 1 0 1 1 1 1 0 1 1 1 0 0 0];

 


%% Synchronization header (SHR)

% Preamble is 4 octets, all set to 0.
preamble = zeros(4*8, 1);


SHR = [preamble];

%% PHY protocol data unit:
PPDU = [SHR;];

% pre-allocate matrix for performance
chips = zeros(chipLen, length(PPDU)/4);
for idx = 1:length(PPDU)/4
  %% Bit to symbol mapping
  currBits = PPDU(1+(idx-1)*4 : idx*4);
  symbol = bi2de(currBits');
  
  %% Symbol to chip mapping                            
	chips(:, idx) = chipMap(1+symbol, :)'; % +1 for 1-based indexing
end

%% O-QPSK modululation (part 1)
% split two 2 parallel streams, also map [0, 1] to [-1, 1]
oddChips  = chips(1:2:end)*2-1;
evenChips = chips(2:2:end)*2-1;

% Half-sine pulse filtering for 2450 MHz
pulse = sin(0:pi/spc:(spc-1)*pi/spc); % Half-period sine wave
filteredReal = pulse' * oddChips;     % each column is now a filtered pulse
filteredImag = pulse' * evenChips;    % each column is now a filtered pulse  

% O-QPSK modululation (part 2)
re = [filteredReal(:);         zeros(round(spc/2), 1)];
im = [zeros(round(spc/2), 1);  filteredImag(:)];

gnr_sig = complex(re, im); % ideal generated signal

rx_sig=rect_sig(1:length(gnr_sig(1:end-2))); % received signal

%% amplitude estimation (not used)
ampl=abs(rx_sig.' * conj(gnr_sig(1:end-2)))/length(gnr_sig(1:end-2));

%% phase estimation
[acor,~] = xcorr(rx_sig(1:end),gnr_sig(1:end-2),'biased');
[~,I]=max(abs(acor));
phase=angle(acor(I));

end