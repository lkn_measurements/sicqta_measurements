%% Function to calculate SINR
% Inputs:
% collision_signal : Signal for which SINR is to be calculated
% freq_shifted_regen : Signal model (used to obtain signal power)
% 
% Outputs:
% SNRdB : SINR in dB
function [SNRdB]=snr_calc(collision_signal,freq_shifted_regen)
%% Calculate lag and phase offset 
[acor,lag] = xcorr(collision_signal,freq_shifted_regen);
[~,I]=max((acor));
lag_req=lag(I);
phase_offset=angle(acor(I));
spc=4;

% add phase offset to model 
H = comm.PhaseFrequencyOffset('SampleRate', spc*1e6,'PhaseOffset',phase_offset*180/pi);
fprintf('Estimated phase offset = %.3f rad\n', phase_offset);
phase_freq_shifted_regen=H(freq_shifted_regen);

data_signal=collision_signal(lag_req+1:lag_req+length(phase_freq_shifted_regen));
%% snr model
% --- Check the signal power. 
L=length(phase_freq_shifted_regen(:));
% Measure average power of the signal
Ps = sum(abs(phase_freq_shifted_regen(:)).^2)/L;    
% Measure the noise power
n=data_signal-phase_freq_shifted_regen;
Pn = sum(abs(n(:)).^2)/L;
SNR = Ps/Pn;    % SNR computation
SNRdB = 10*log10(SNR);    %SNR in dB

end