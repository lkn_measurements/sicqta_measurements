%% Function to perform successive interference cancellation
% Inputs:
% packets : all packets
% freq_shifted_regen : model of signal to be cancelled
% slot_idx : position of slot from which the signal is to be cancelled
% spc : samples per chip
% 
% Outputs:
% cancelled_signal : Signal remaining after cancellation
% phase_offset : estimated phase offset(not used)


function [cancelled_signal,phase_offset]=sic(packets,freq_shifted_regen,slot_idx,spc)
%% Get the replica slot
collision_signal=packets(:,slot_idx);
figure;
plot(1:size(collision_signal),abs(collision_signal))
title('collision slot');

%% Calculate lag and phase offset 
[acor,lag] = xcorr(collision_signal,freq_shifted_regen);
[~,I]=max((acor));
lag_req=lag(I);
phase_offset=angle(acor(I));

% Add phase offset
H = comm.PhaseFrequencyOffset('SampleRate', spc*1e6,'PhaseOffset',phase_offset*180/pi);
fprintf('Estimated phase offset = %.3f rad\n', phase_offset);
phase_freq_shifted_regen=H(freq_shifted_regen);

if lag_req >=0
    %% Subtract signal (interference cancellation)
    sig2=[zeros(lag_req,1); phase_freq_shifted_regen;zeros(length(collision_signal)-length(phase_freq_shifted_regen)-lag_req,1)];

    figure;
    subplot(1,2,1)
    plot(abs(sig2))
    hold on
    plot(abs(collision_signal));
    title('Magnitude comparison of regenerated signal and received signal');
    subplot(1,2,2)
    plot(angle(sig2))
    hold on
    plot(angle(collision_signal));
    title('Angle comparison of regenerated signal and received signal');
    
    % Signal subtarction
    cancelled_signal=collision_signal-sig2;

    figure;
    plot(1:size(collision_signal),abs(collision_signal))
    title('Signal Magnitude before cancellation')
    xlabel('Samples'),ylabel('Amplitude');
    figure;
    plot(1:size(cancelled_signal),abs(cancelled_signal))
    title('Signal Magnitude after cancellation')
    xlabel('Samples'),ylabel('Amplitude');
    
    %% Detect residual signal in cancelled signal using correlation

    prbdet = comm.PreambleDetector(phase_freq_shifted_regen(1:end),'Threshold',50);

    [~,detmet] = prbdet(cancelled_signal);
    [~,detmet_test] = prbdet(collision_signal);
    figure;
    plot(1:size(detmet),detmet)
    hold on 
    plot(1:size(detmet_test),detmet_test)
    title('Packet detection before and after cancellation');

else
cancelled_signal=zeros(length(collision_signal),1);
end