%% Main Program to process the SIC scenario 654321 
clc
clearvars
close all
figures=[];

%% Define parameters 
spc=4; %samples per chip
packet_length=17026; %133 byte packet in samples
slot_length=60000;%15 ms in samples
packets=[]; %each column contains a data burst
num_frames=52;
load('packets.mat')

%% Start processing packets
pclean=zeros(num_frames,6); % contains 1s in the positions of correctly decoded clean packets
pdec=zeros(num_frames,5); % contains 1s in the positions of correctly decoded packets obtained after SIC
SNR=zeros(num_frames,6);
BER=ones(num_frames,5);

iter=0;
for curr_idx=6:11:567 %frame=65432111111,frame length=11
    iter=iter+1;
    %% Extract clean packets
    [phaseCompensatedOQPSK,MPDU,coarseFrequencyOffset,~,pass,pkt_f]=rx_chain(packets(:,curr_idx+5),spc,packet_length);
    pclean(iter,6)=pass;
    
    % Modelling the signal
    regen_sig=model_signal(phaseCompensatedOQPSK,MPDU,spc); % model channel effect(averaging technique)
    freq_shift=(0:length(regen_sig)) ./ (spc*1e6) .* coarseFrequencyOffset;
    freq_shifted_regen_f=regen_sig.*exp(1i.*2.*pi.*freq_shift(1:length(regen_sig)).'); % add frequency offset

    [phaseCompensatedOQPSK,MPDU,coarseFrequencyOffset,~,pass,pkt_e]=rx_chain(packets(:,curr_idx+4),spc,packet_length);
    pclean(iter,5)=pass;
    % Modelling the signal
    regen_sig=model_signal(phaseCompensatedOQPSK,MPDU,spc);
    freq_shift=(0:length(regen_sig)) ./ (spc*1e6) .* coarseFrequencyOffset;
    freq_shifted_regen_e=regen_sig.*exp(1i.*2.*pi.*freq_shift(1:length(regen_sig)).');
    
    [phaseCompensatedOQPSK,MPDU,coarseFrequencyOffset,~,pass,pkt_d]=rx_chain(packets(:,curr_idx+3),spc,packet_length);
    pclean(iter,4)=pass;
    % Modelling the signal
    regen_sig=model_signal(phaseCompensatedOQPSK,MPDU,spc);
    freq_shift=(0:length(regen_sig)) ./ (spc*1e6) .* coarseFrequencyOffset;
    freq_shifted_regen_d=regen_sig.*exp(1i.*2.*pi.*freq_shift(1:length(regen_sig)).');
    
    [phaseCompensatedOQPSK,MPDU,coarseFrequencyOffset,~,pass,pkt_c]=rx_chain(packets(:,curr_idx+2),spc,packet_length);
    pclean(iter,3)=pass;
    % Modelling the signal
    regen_sig=model_signal(phaseCompensatedOQPSK,MPDU,spc);
    freq_shift=(0:length(regen_sig)) ./ (spc*1e6) .* coarseFrequencyOffset;
    freq_shifted_regen_c=regen_sig.*exp(1i.*2.*pi.*freq_shift(1:length(regen_sig)).');
    
    [phaseCompensatedOQPSK,MPDU,coarseFrequencyOffset,~,pass,pkt_b]=rx_chain(packets(:,curr_idx+1),spc,packet_length);
    pclean(iter,2)=pass;
    % Modelling the signal
    regen_sig=model_signal(phaseCompensatedOQPSK,MPDU,spc);
    freq_shift=(0:length(regen_sig)) ./ (spc*1e6) .* coarseFrequencyOffset;
    freq_shifted_regen_b=regen_sig.*exp(1i.*2.*pi.*freq_shift(1:length(regen_sig)).');
    
    %% Receiver chain and Modelling
    [phaseCompensatedOQPSK,MPDU,coarseFrequencyOffset,phase_est,pass,pkt_a]=rx_chain(packets(:,curr_idx),spc,packet_length);
    pclean(iter,1)=pass;
    close all
    if(pass==1) %if CRC passed
        
        % Modelling the signal
        regen_sig=model_signal(phaseCompensatedOQPSK,MPDU,spc);
        freq_shift=(0:length(regen_sig)) ./ (spc*1e6) .* coarseFrequencyOffset;
        freq_shifted_regen=regen_sig.*exp(1i.*2.*pi.*freq_shift(1:length(regen_sig)).');
        freq_shifted_regen_a=freq_shifted_regen;
        
        % Calculate SNR 
        [SNRdB]=snr_calc(packets(:,curr_idx),freq_shifted_regen_a);
        SNR(iter,1)=SNRdB;      
        
        %% Cancel from other slots
        for canc=curr_idx-1:-1:curr_idx-5
            [packets(:,canc),~]=sic(packets,freq_shifted_regen,canc,spc);
            close all
        end
        canc=curr_idx-1;
        cancelled_signal=packets(:,canc);
        
        % Decode clean packet obtained after SIC
        [phaseCompensatedOQPSK,MPDU,coarseFrequencyOffset,~,pass,pkt]=rx_chain(cancelled_signal,spc,packet_length);
        pdec(iter,1)=pass;
        
        % Calculate BER  
        BER(iter,1)=ber(pkt,pkt_b); % Calculate BER compared to the clean slot
        % Calculate SNR 
        [SNRdB]=snr_calc(cancelled_signal,freq_shifted_regen_b);
        SNR(iter,2)=SNRdB;
       
        if(pass==1)
            % Modelling the signal
            regen_sig=model_signal(phaseCompensatedOQPSK,MPDU,spc);
            freq_shift=(0:length(regen_sig)) ./ (spc*1e6) .* coarseFrequencyOffset;
            freq_shifted_regen=regen_sig.*exp(1i.*2.*pi.*freq_shift(1:length(regen_sig)).');     
            
            %% Cancel from other slots
            for canc=curr_idx-2:-1:curr_idx-5
                [packets(:,canc),~]=sic(packets,freq_shifted_regen,canc,spc);
                close all
            end
            canc=curr_idx-2;
            cancelled_signal=packets(:,canc);
            % Decode clean packet obtained after SIC
            [phaseCompensatedOQPSK,MPDU,coarseFrequencyOffset,~,pass,pkt]=rx_chain(cancelled_signal,spc,packet_length);
            pdec(iter,2)=pass;
            % Calculate BER
            BER(iter,2)=ber(pkt,pkt_c);
            % Calcualte SNR
            [SNRdB]=snr_calc(cancelled_signal,freq_shifted_regen_c);
            SNR(iter,3)=SNRdB;
            if(pass==1)
                % Modelling the signal
                regen_sig=model_signal(phaseCompensatedOQPSK,MPDU,spc);
                freq_shift=(0:length(regen_sig)) ./ (spc*1e6) .* coarseFrequencyOffset;
                freq_shifted_regen=regen_sig.*exp(1i.*2.*pi.*freq_shift(1:length(regen_sig)).');  
                
                %% Cancel from other slots
                for canc=curr_idx-3:-1:curr_idx-5
                    [packets(:,canc),~]=sic(packets,freq_shifted_regen,canc,spc);
                    close all
                end
                canc=curr_idx-3;
                cancelled_signal=packets(:,canc);
                
                % Decode clean packet obtained after SIC
                [phaseCompensatedOQPSK,MPDU,coarseFrequencyOffset,~,pass,pkt]=rx_chain(cancelled_signal,spc,packet_length);
                pdec(iter,3)=pass;
                
                % Calcualte BER
                BER(iter,3)=ber(pkt,pkt_d);
                % Calculate SNR
                [SNRdB]=snr_calc(cancelled_signal,freq_shifted_regen_d);
                SNR(iter,4)=SNRdB;
                if(pass==1)
                    % Modelling the signal
                    regen_sig=model_signal(phaseCompensatedOQPSK,MPDU,spc);
                    freq_shift=(0:length(regen_sig)) ./ (spc*1e6) .* coarseFrequencyOffset;
                    freq_shifted_regen=regen_sig.*exp(1i.*2.*pi.*freq_shift(1:length(regen_sig)).');

                    %% Cancel from other slots
                    for canc=curr_idx-4:-1:curr_idx-5
                        [packets(:,canc),~]=sic(packets,freq_shifted_regen,canc,spc);
                        close all
                    end
                    canc=curr_idx-4;
                    cancelled_signal=packets(:,canc);
                    % Decode clean packet obtained after SIC
                    [phaseCompensatedOQPSK,MPDU,coarseFrequencyOffset,~,pass,pkt]=rx_chain(cancelled_signal,spc,packet_length);
                    pdec(iter,4)=pass;
                    
                    % Calculate BER
                    BER(iter,4)=ber(pkt,pkt_e);
                    % Calculate SNR
                    [SNRdB]=snr_calc(cancelled_signal,freq_shifted_regen_e);
                    SNR(iter,5)=SNRdB;
                    if(pass==1)
                        % Modelling the signal
                        regen_sig=model_signal(phaseCompensatedOQPSK,MPDU,spc);
                        freq_shift=(0:length(regen_sig)) ./ (spc*1e6) .* coarseFrequencyOffset;
                        freq_shifted_regen=regen_sig.*exp(1i.*2.*pi.*freq_shift(1:length(regen_sig)).');
                        
                        %% Cancel from other slots
                        for canc=curr_idx-5:-1:curr_idx-5
                            [packets(:,canc),~]=sic(packets,freq_shifted_regen,canc,spc);
                            close all
                        end
                        canc=curr_idx-5;
                        cancelled_signal=packets(:,canc);
                        
                        % Decode clean packet obtained after SIC
                        [phaseCompensatedOQPSK,MPDU,coarseFrequencyOffset,~,pass,pkt]=rx_chain(cancelled_signal,spc,packet_length);
                        pdec(iter,5)=pass;
                        
                        % Calculate BER
                        BER(iter,5)=ber(pkt,pkt_f);
                        % Calculate SNR
                        [SNRdB]=snr_calc(cancelled_signal,freq_shifted_regen_f);
                        SNR(iter,6)=SNRdB;
                    end
                end
            end
        end
    end
end
        
save('results.mat','pclean','pdec','SNR','BER');
