%% Function to print the expected practical throughput for 4,3 or 2 users and the resolution probability for each tree splitting scenario
% Inputs: 
% ch_gain:A constant number - Channel gain(proportional to the received signal strength)
% ph_n_var:n array with two values representing the variance of the real and imaginary parts of the complex phase noise  
% est_err:A constant number representing the channel estimation/implementation error
% n_var:Constant number representing the variance of the Gaussian channel noise
% eg. ch_gain=0.25, ph_n_var=[5e-4 0.01], est_err=0.2, n_var=2e-6 
% Assumption: 
% 1. All users have the same channel gains and phase noise variances
% 2. Time diversity calculation is completely analytical (uses theoretical resolution probability values)

% Steps to obtain snrvsber mapping
% bertool
% set Channel type=Rician, EbNo range = -20:40, K-factor=4, Modulation type OQPSK
% Click plot
% Right click on data and export to workspace (variables: ebno0 and ber0)
% sinr_db_th=ebno0-(10*log10(16));
% ber_th=ber0;
% save('snrvsber.mat','sinr_db_th','ber_th');

function [thruput_2,thruput_3,thruput_4]=sic_paper(ch_gain,ph_n_var,est_err,n_var)
pkt_length=128; %length of packet in bytes
% Calculate the theoretical SINR
var1=repmat(ph_n_var(1),[1,4]);
var2=repmat(ph_n_var(2),[1,4]);
sinr_db_pr=[];
xvals_h=repmat(ch_gain,[1,4]);

pn_phase_th=var1.*(xvals_h.^2)+var2.*(xvals_h.^2); %phase noise power
alpha=est_err; 
xah=((alpha.*xvals_h).^2)+var1.*((alpha.*xvals_h).^2)+var2.*((alpha.*xvals_h).^2); %Power due to imperfect cancellation
pn_n_th=n_var;
Ps = xvals_h.^2; %Received signal power

for iter=1:4
    Pn=(sum(pn_phase_th(iter:-1:1))+(pn_n_th*iter))+sum([0.0001 xah(iter-1:-1:1)]); %Sum of phase noise power , Gaussian noise power and interference power due to imperfect cancellation
    sinr_pr=Ps(iter)/Pn;
    sinr_db_pr=[sinr_db_pr 10*log10(sinr_pr)];
end
load('snrvsber.mat'); %Contains snr to ber mapping for Rician channel with Kfactor=4
ber_pr = interp1(sinr_db_th,ber_th,sinr_db_pr);
p_21 = (1-ber_pr(2))^(pkt_length*8); %probability of 2 given 1
p_321 = (1-ber_pr(3))^(pkt_length*8); %probability of 3 given 1 & 2 are successfully decoded
p_4321 = (1-ber_pr(4))^(pkt_length*8); %probability of 4 given 1 & 2 & 3 are successfully decoded

%2users
scen_2=[2221 2201 221 2021 2001 201 21]; 
pocc_2=[2^-4 2^-4 2^-3 2^-4 2^-4 2^-3 2^-1];
pres_2=[1-(1-p_21)^3 1-(1-p_21)^2 1-(1-p_21)^2 1-(1-p_21)^2 p_21 p_21 p_21]; %Resolution probability
thru_2=[0.5 0.5 2/3 0.5 0.5 2/3 1];
thruput_2=sum(pocc_2.*pres_2.*thru_2);
fprintf('   2 Users --> Throughput=%d\n',thruput_2);
fprintf('   Scenario   Occurrence probability   Resolution probability\n');
    for rr = 1 : length(scen_2)
        fprintf( '\t%6s\t\t%6d\t\t\t%6d\n', num2str(scen_2(rr)),pocc_2(rr),pres_2(rr) ); 
    end


%3users 
scen_3=[3321 3311 3021 30121 3221 3201 321 3121 3101 311];
pocc_3=[2^-4 2^-4 2^-4 2^-4 3*2^-5 3*2^-5 6*2^-5 3*2^-5 3*2^-5 6*2^-5];
pres_3=[(1-(1-p_321)^2)*p_21 (1-(1-p_321)^2) p_321*p_21 p_21 p_321*(1-(1-p_21)^2) p_321*p_21 p_321*p_21 p_21 p_321 p_321];
thru_3=[0.75 0.75 0.75 0.6 0.75 0.75 1 0.75 0.75 1];
thruput_3=sum(pocc_3.*pres_3.*thru_3);
fprintf('   3 Users --> Throughput=%d\n',thruput_3);
fprintf('   Scenario   Occurrence probability   Resolution probability\n');
    for rr = 1 : length(scen_3)
        fprintf( '\t%6s\t\t%6d\t\t\t%6d\n', num2str(scen_3(rr)),pocc_3(rr),pres_3(rr) ); 
    end
%     
%4users
scen_4=[4421 40211 4321 4311 422121 42121 422101 42101 420121 420101 4121 4111];
pocc_4=[2^-4 2^-4 4*2^-5 4*2^-5 6*2^-7 12*2^-7 6*2^-7 12*2^-7 6*2^-7 6*2^-7 4*2^-5 4*2^-5];
pres_4=[(1-(1-p_4321)^2)*p_21 p_4321*p_21 p_4321*p_321*p_21 p_4321*p_321 (1-(1-p_21)^2)*p_21 p_21*p_21 (1-(1-p_21)^2)*p_4321 p_21*p_4321 p_21*p_21 p_21*p_4321 p_21*p_4321 p_4321];
thru_4=[4/5 4/5 1 1 4/6 4/5 4/6 4/5 4/6 4/6 1 1];
thruput_4=sum(pocc_4.*pres_4.*thru_4);
fprintf('   4 Users --> Throughput=%d\n',thruput_4);
fprintf('   Scenario   Occurrence probability   Resolution probability\n');
    for rr = 1 : length(scen_4)
        fprintf( '\t%6s\t\t%6d\t\t\t%6d\n', num2str(scen_4(rr)),pocc_4(rr),pres_4(rr) ); 
    end

end