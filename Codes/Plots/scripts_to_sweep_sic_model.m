clear

ch_gain=0.25;
ph_n_var=[5e-4 0.01];
est_err=0.3;
n_var=2e-6;


sweep = 0:0.001:0.4;
thruput_2 = zeros(1,length(sweep));
thruput_3 = zeros(1,length(sweep));
thruput_4 = zeros(1,length(sweep));

parfor i = 1:length(sweep)
    [thruput_2(i),thruput_3(i),thruput_4(i)] = sic_paper(ch_gain,[5e-4 sweep(i)],10^-5,10^-5);
end

thruput_12 = zeros(1,length(sweep));
thruput_13 = zeros(1,length(sweep));
thruput_14 = zeros(1,length(sweep));

parfor i = 1:length(sweep)
    [thruput_12(i),thruput_13(i),thruput_14(i)] = sic_paper(ch_gain,[5e-4 sweep(i)],est_err,n_var);
end

save('sweep_phase_ideal.mat');